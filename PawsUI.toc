## Interface: 70300
## Title: PawsUI
## Author: Orangepaw3
## Version: 0.1
## Notes: A Replacement User Interface
## Dependencies: ElvUI, ElvUI_Config, AddOnSkins, ElvUI_BenikUI, ElvUI_CustomTweaks, ElvUI_SLE
## SavedVariables: PUi
## DefaultState: Enabled
## X-Category: User Interface
## X-Build: 1

## Libs

## Main Files
init.lua

## Modules
