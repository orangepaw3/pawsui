--Don't worry about this
local addon, ns = ...
local Version = GetAddOnMetadata(addon, "Version")

--Cache Lua / WoW API
local format = string.format
local GetCVarBool = GetCVarBool
local ReloadUI = ReloadUI
local StopMusic = StopMusic

-- These are things we do not cache
-- GLOBALS: PluginInstallStepComplete, PluginInstallFrame

--Change this line and use a unique name for your plugin.
local MyPluginName = "Paws UI"

--Create references to ElvUI internals
local E, L, V, P, G = unpack(ElvUI)

--Create reference to LibElvUIPlugin
local EP = LibStub("LibElvUIPlugin-1.0")

--Create a new ElvUI module so ElvUI can handle initialization when ready
local mod = E:NewModule(MyPluginName, "AceHook-3.0", "AceEvent-3.0", "AceTimer-3.0");

--This function will hold your layout settings
local function SetupLayout(layout)
	--[[
	--	PUT YOUR EXPORTED PROFILE/SETTINGS BELOW HERE --]]
		E.db["databars"]["artifact"]["orientation"] = "HORIZONTAL"
E.db["databars"]["artifact"]["textFormat"] = "PERCENT"
E.db["databars"]["artifact"]["height"] = 13
E.db["databars"]["artifact"]["width"] = 230
E.db["databars"]["honor"]["enable"] = false
E.db["currentTutorial"] = 3
E.db["sle"]["media"]["fonts"]["objective"]["font"] = "Durandal Light"
E.db["sle"]["media"]["fonts"]["zone"]["font"] = "Durandal Light"
E.db["sle"]["media"]["fonts"]["objectiveHeader"]["font"] = "Durandal Light"
E.db["sle"]["media"]["fonts"]["editbox"]["font"] = "Durandal Light"
E.db["sle"]["media"]["fonts"]["gossip"]["font"] = "Durandal Light"
E.db["sle"]["media"]["fonts"]["mail"]["font"] = "Durandal Light"
E.db["sle"]["media"]["fonts"]["questFontSuperHuge"]["font"] = "Durandal Light"
E.db["sle"]["Armory"]["Character"]["Stats"]["ItemLevel"]["size"] = 22
E.db["sle"]["Armory"]["Character"]["Stats"]["ItemLevel"]["font"] = "Continuum Medium"
E.db["sle"]["Armory"]["Character"]["Stats"]["IlvlFull"] = true
E.db["sle"]["Armory"]["Character"]["Level"]["Font"] = "Bui PrototypeRU"
E.db["sle"]["Armory"]["Character"]["Backdrop"]["Overlay"] = false
E.db["sle"]["Armory"]["Character"]["Backdrop"]["SelectedBG"] = "Castle"
E.db["sle"]["raidmarkers"]["enable"] = false
E.db["bags"]["countFontSize"] = 12
E.db["bags"]["countFont"] = "Bui Tukui"
E.db["bags"]["itemLevelFont"] = "Bui Tukui"
E.db["bags"]["countFontOutline"] = "OUTLINE"
E.db["bags"]["itemLevelFontSize"] = 12
E.db["bags"]["itemLevelFontOutline"] = "OUTLINE"
E.db["bags"]["bankWidth"] = 259
E.db["bags"]["bankSize"] = 26
E.db["bags"]["bagSize"] = 26
E.db["bags"]["bagWidth"] = 259
E.db["hideTutorial"] = true
E.db["chat"]["tabFontOutline"] = "OUTLINE"
E.db["chat"]["tabFont"] = "Bui Tukui"
E.db["chat"]["font"] = "Bui Tukui"
E.db["chat"]["panelBackdrop"] = "LEFT"
E.db["chat"]["panelHeight"] = 140
E.db["chat"]["editBoxPosition"] = "ABOVE_CHAT"
E.db["chat"]["fontSize"] = 12
E.db["chat"]["tapFontSize"] = 12
E.db["chat"]["panelWidth"] = 323
E.db["movers"]["PetAB"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-4,-1"
E.db["movers"]["ElvUF_RaidMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,637,230"
E.db["movers"]["LeftChatMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,24,48"
E.db["movers"]["BossButton"] = "BOTTOM,ElvUIParent,BOTTOM,303,108"
E.db["movers"]["ZoneAbility"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-444,89"
E.db["movers"]["ElvUF_RaidpetMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,4,736"
E.db["movers"]["PlayerPortraitMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,486,183"
E.db["movers"]["ElvUF_FocusMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-454,-363"
E.db["movers"]["ClassBarMover"] = "BOTTOM,ElvUIParent,BOTTOM,-126,166"
E.db["movers"]["MicrobarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,-1"
E.db["movers"]["BuiMiddleDtMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,0"
E.db["movers"]["ElvUIBankMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,161,190"
E.db["movers"]["ElvUF_Raid40Mover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,1223,385"
E.db["movers"]["ElvAB_1"] = "BOTTOM,ElvUIParent,BOTTOM,-250,232"
E.db["movers"]["ElvAB_2"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,346,24"
E.db["movers"]["TalkingHeadFrameMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-338"
E.db["movers"]["ElvAB_3"] = "BOTTOM,ElvUIParent,BOTTOM,249,229"
E.db["movers"]["ElvAB_5"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-322,19"
E.db["movers"]["ArtifactBarMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-373,4"
E.db["movers"]["ElvUF_TargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,126,183"
E.db["movers"]["ElvUF_PlayerCastbarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,404,239"
E.db["movers"]["ObjectiveFrameMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,152,-136"
E.db["movers"]["ArenaHeaderMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-52,-357"
E.db["movers"]["ShiftAB"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,222,372"
E.db["movers"]["TargetPortraitMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-486,181"
E.db["movers"]["ElvUF_PartyMover"] = "TOPRIGHT,ElvUIParent,BOTTOMLEFT,1631,285"
E.db["movers"]["ElvUF_TargetCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,128,88"
E.db["movers"]["TooltipMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,24,190"
E.db["movers"]["ElvUIBagMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-40,193"
E.db["movers"]["BossHeaderMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-254,-295"
E.db["movers"]["PlayerPowerBarMover"] = "BOTTOM,ElvUIParent,BOTTOM,-266,430"
E.db["movers"]["ElvUF_PetMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,469,145"
E.db["movers"]["RightChatMover"] = "BOTTOMRIGHT,UIParent,BOTTOMRIGHT,0,19"
E.db["movers"]["ElvUF_PlayerMover"] = "BOTTOM,ElvUIParent,BOTTOM,-126,183"
E.db["movers"]["ElvUF_TargetTargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,378,193"
E.db["movers"]["MinimapMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-57,-23"
E.db["general"]["backdropcolor"]["r"] = 0.025
E.db["general"]["backdropcolor"]["g"] = 0.025
E.db["general"]["backdropcolor"]["b"] = 0.025
E.db["general"]["vendorGrays"] = true
E.db["general"]["interruptAnnounce"] = "SAY"
E.db["general"]["minimap"]["locationFont"] = "Bui Tukui"
E.db["general"]["minimap"]["icons"]["classHall"]["scale"] = 0.7
E.db["general"]["minimap"]["icons"]["classHall"]["yOffset"] = 50
E.db["general"]["minimap"]["icons"]["classHall"]["position"] = "BOTTOMRIGHT"
E.db["general"]["vendorGraysDetails"] = true
E.db["general"]["font"] = "Bui Tukui"
E.db["general"]["bonusObjectivePosition"] = "AUTO"
E.db["general"]["backdropfadecolor"]["a"] = 0.75
E.db["general"]["backdropfadecolor"]["r"] = 0.086
E.db["general"]["backdropfadecolor"]["g"] = 0.109
E.db["general"]["backdropfadecolor"]["b"] = 0.149
E.db["general"]["objectiveFrameHeight"] = 724
E.db["general"]["topPanel"] = true
E.db["general"]["autoAcceptInvite"] = true
E.db["tooltip"]["font"] = "Bui Tukui"
E.db["tooltip"]["healthBar"]["font"] = "Bui Tukui"
E.db["tooltip"]["visibility"]["combat"] = true
E.db["tooltip"]["fontSize"] = 12
E.db["dashboards"]["tokens"]["enableTokens"] = false
E.db["dashboards"]["professions"]["enableProfessions"] = false
E.db["dashboards"]["system"]["enableSystem"] = false
E.db["auras"]["debuffs"]["countFontSize"] = 12
E.db["auras"]["debuffs"]["durationFontSize"] = 12
E.db["auras"]["fontSize"] = 12
E.db["auras"]["buffs"]["countFontSize"] = 12
E.db["auras"]["buffs"]["durationFontSize"] = 12
E.db["auras"]["font"] = "Bui Tukui"
E.db["unitframe"]["font"] = "Bui Tukui"
E.db["unitframe"]["colors"]["classbackdrop"] = true
E.db["unitframe"]["colors"]["health"]["r"] = 0.18039215686275
E.db["unitframe"]["colors"]["health"]["g"] = 0.18039215686275
E.db["unitframe"]["colors"]["health"]["b"] = 0.18039215686275
E.db["unitframe"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["smoothbars"] = true
E.db["unitframe"]["fontSize"] = 14
E.db["unitframe"]["units"]["pet"]["castbar"]["enable"] = false
E.db["unitframe"]["units"]["tank"]["enable"] = false
E.db["unitframe"]["units"]["targettarget"]["power"]["enable"] = false
E.db["unitframe"]["units"]["targettarget"]["height"] = 28
E.db["unitframe"]["units"]["assist"]["enable"] = false
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["xOffset"] = 59
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["text_format"] = "[namecolor][name:short]"
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["yOffset"] = 17
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["enable"] = true
E.db["unitframe"]["units"]["boss"]["customTexts"]["nom"]["size"] = 14
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["xOffset"] = -48
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["text_format"] = "[healthcolor][health:percent] ||"
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["yOffset"] = 17
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["enable"] = true
E.db["unitframe"]["units"]["boss"]["customTexts"]["percent"]["size"] = 14
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["xOffset"] = 11
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["text_format"] = "[healthcolor][health:current] ||"
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["yOffset"] = 17
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["enable"] = true
E.db["unitframe"]["units"]["boss"]["customTexts"]["vie"]["size"] = 14
E.db["unitframe"]["units"]["boss"]["width"] = 188
E.db["unitframe"]["units"]["boss"]["name"]["text_format"] = ""
E.db["unitframe"]["units"]["boss"]["power"]["text_format"] = ""
E.db["unitframe"]["units"]["boss"]["height"] = 25
E.db["unitframe"]["units"]["boss"]["health"]["text_format"] = ""
E.db["unitframe"]["units"]["raid40"]["rdebuffs"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["raid40"]["width"] = 82
E.db["unitframe"]["units"]["raid40"]["height"] = 17
E.db["unitframe"]["units"]["target"]["portrait"]["rotation"] = 3
E.db["unitframe"]["units"]["target"]["portrait"]["width"] = 0
E.db["unitframe"]["units"]["target"]["portrait"]["camDistanceScale"] = 3.28
E.db["unitframe"]["units"]["target"]["portrait"]["yOffset"] = -0.01
E.db["unitframe"]["units"]["target"]["castbar"]["height"] = 32
E.db["unitframe"]["units"]["target"]["castbar"]["icon"] = false
E.db["unitframe"]["units"]["target"]["castbar"]["width"] = 208
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["xOffset"] = 59
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["text_format"] = "[health:current]"
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["yOffset"] = -1
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["enable"] = true
E.db["unitframe"]["units"]["target"]["customTexts"]["vie"]["size"] = 36
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["xOffset"] = -9
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["text_format"] = "[health:percent]|| "
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["yOffset"] = 8
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["font"] = "Bui Prototype"
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["enable"] = true
E.db["unitframe"]["units"]["target"]["customTexts"]["percent"]["size"] = 13
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["xOffset"] = -27
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["text_format"] = "[namecolor][name:short]"
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["yOffset"] = -8
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["enable"] = true
E.db["unitframe"]["units"]["target"]["customTexts"]["Nam"]["size"] = 21
E.db["unitframe"]["units"]["target"]["width"] = 242
E.db["unitframe"]["units"]["target"]["health"]["text_format"] = ""
E.db["unitframe"]["units"]["target"]["name"]["text_format"] = ""
E.db["unitframe"]["units"]["target"]["power"]["height"] = 7
E.db["unitframe"]["units"]["target"]["power"]["text_format"] = ""
E.db["unitframe"]["units"]["target"]["height"] = 45
E.db["unitframe"]["units"]["target"]["buffs"]["yOffset"] = -106
E.db["unitframe"]["units"]["target"]["aurabar"]["enable"] = false
E.db["unitframe"]["units"]["raid"]["buffIndicator"]["size"] = 7
E.db["unitframe"]["units"]["raid"]["rdebuffs"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["raid"]["rdebuffs"]["fontSize"] = 7
E.db["unitframe"]["units"]["raid"]["rdebuffs"]["size"] = 14
E.db["unitframe"]["units"]["raid"]["growthDirection"] = "RIGHT_UP"
E.db["unitframe"]["units"]["raid"]["width"] = 79
E.db["unitframe"]["units"]["raid"]["height"] = 40
E.db["unitframe"]["units"]["party"]["debuffs"]["anchorPoint"] = "CENTER"
E.db["unitframe"]["units"]["party"]["debuffs"]["sizeOverride"] = 21
E.db["unitframe"]["units"]["party"]["debuffs"]["xOffset"] = 15
E.db["unitframe"]["units"]["party"]["debuffs"]["yOffset"] = -13
E.db["unitframe"]["units"]["party"]["debuffs"]["perrow"] = 3
E.db["unitframe"]["units"]["party"]["rdebuffs"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["party"]["growthDirection"] = "LEFT_DOWN"
E.db["unitframe"]["units"]["party"]["buffIndicator"]["size"] = 6
E.db["unitframe"]["units"]["party"]["healPrediction"] = true
E.db["unitframe"]["units"]["party"]["width"] = 79
E.db["unitframe"]["units"]["party"]["height"] = 53
E.db["unitframe"]["units"]["player"]["debuffs"]["enable"] = false
E.db["unitframe"]["units"]["player"]["debuffs"]["attachTo"] = "BUFFS"
E.db["unitframe"]["units"]["player"]["portrait"]["width"] = 15
E.db["unitframe"]["units"]["player"]["portrait"]["camDistanceScale"] = 4
E.db["unitframe"]["units"]["player"]["portrait"]["yOffset"] = -0.03
E.db["unitframe"]["units"]["player"]["aurabar"]["enable"] = false
E.db["unitframe"]["units"]["player"]["castbar"]["height"] = 54
E.db["unitframe"]["units"]["player"]["castbar"]["width"] = 146
E.db["unitframe"]["units"]["player"]["castbar"]["icon"] = false
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["xOffset"] = -5
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["text_format"] = " || [health:percent]"
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["yOffset"] = 11
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["font"] = "Bui Prototype"
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["enable"] = true
E.db["unitframe"]["units"]["player"]["customTexts"]["percent"]["size"] = 15
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["xOffset"] = -75
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["text_format"] = "[health:current]"
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["yOffset"] = -1
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["enable"] = true
E.db["unitframe"]["units"]["player"]["customTexts"]["vie"]["size"] = 44
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["xOffset"] = 128
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["text_format"] = "[powercolor][power:percent]"
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["yOffset"] = 245
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["enable"] = true
E.db["unitframe"]["units"]["player"]["customTexts"]["energy"]["size"] = 18
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["attachTextTo"] = "Health"
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["xOffset"] = 5
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["text_format"] = "[namecolor][name]"
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["yOffset"] = -9
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["font"] = "Bui Tukui"
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["justifyH"] = "CENTER"
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["fontOutline"] = "OUTLINE"
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["enable"] = true
E.db["unitframe"]["units"]["player"]["customTexts"]["nom"]["size"] = 27
E.db["unitframe"]["units"]["player"]["health"]["text_format"] = ""
E.db["unitframe"]["units"]["player"]["width"] = 242
E.db["unitframe"]["units"]["player"]["power"]["height"] = 7
E.db["unitframe"]["units"]["player"]["power"]["enable"] = false
E.db["unitframe"]["units"]["player"]["height"] = 45
E.db["unitframe"]["units"]["player"]["buffs"]["attachTo"] = "FRAME"
E.db["unitframe"]["units"]["player"]["classbar"]["detachFromFrame"] = true
E.db["unitframe"]["units"]["player"]["classbar"]["spacing"] = 6
E.db["unitframe"]["units"]["player"]["classbar"]["detachedWidth"] = 156
E.db["unitframe"]["units"]["player"]["classbar"]["height"] = 15
E.db["unitframe"]["units"]["player"]["classbar"]["fill"] = "spaced"
E.db["datatexts"]["minimapPanels"] = false
E.db["datatexts"]["time24"] = true
E.db["datatexts"]["panels"]["BuiRightChatDTPanel"]["right"] = ""
E.db["datatexts"]["panels"]["BuiRightChatDTPanel"]["left"] = ""
E.db["datatexts"]["panels"]["BuiRightChatDTPanel"]["middle"] = ""
E.db["datatexts"]["panels"]["BuiLeftChatDTPanel"]["right"] = ""
E.db["datatexts"]["panels"]["BuiLeftChatDTPanel"]["left"] = ""
E.db["datatexts"]["panels"]["BuiLeftChatDTPanel"]["middle"] = ""
E.db["datatexts"]["panels"]["BuiMiddleDTPanel"]["right"] = "System"
E.db["datatexts"]["panels"]["BuiMiddleDTPanel"]["left"] = "Guild"
E.db["datatexts"]["panels"]["BuiMiddleDTPanel"]["middle"] = "Time"
E.db["datatexts"]["font"] = "Bui Tukui"
E.db["datatexts"]["fontOutline"] = "OUTLINE"
E.db["datatexts"]["leftChatPanel"] = false
E.db["datatexts"]["rightChatPanel"] = false
E.db["actionbar"]["bar3"]["buttonsPerRow"] = 2
E.db["actionbar"]["bar3"]["buttons"] = 12
E.db["actionbar"]["desaturateOnCooldown"] = true
E.db["actionbar"]["fontOutline"] = "OUTLINE"
E.db["actionbar"]["microbar"]["enabled"] = true
E.db["actionbar"]["microbar"]["mouseover"] = true
E.db["actionbar"]["bar2"]["enabled"] = true
E.db["actionbar"]["bar2"]["buttonsPerRow"] = 6
E.db["actionbar"]["bar2"]["mouseover"] = true
E.db["actionbar"]["bar1"]["buttonsPerRow"] = 2
E.db["actionbar"]["bar5"]["mouseover"] = true
E.db["actionbar"]["bar5"]["buttons"] = 12
E.db["actionbar"]["font"] = "Bui Tukui"
E.db["actionbar"]["macrotext"] = true
E.db["actionbar"]["barPet"]["buttonspacing"] = 11
E.db["actionbar"]["barPet"]["buttonsPerRow"] = 10
E.db["actionbar"]["barPet"]["backdrop"] = false
E.db["actionbar"]["barPet"]["buttonsize"] = 18
E.db["actionbar"]["bar4"]["enabled"] = false
E.db["nameplates"]["units"]["ENEMY_NPC"]["eliteIcon"]["enable"] = true
E.db["nameplates"]["font"] = "Bui Tukui"
E.db["benikui"]["installed"] = true
E.db["benikui"]["misc"]["ilevel"]["enable"] = false
E.db["benikui"]["datatexts"]["mail"]["toggle"] = false
E.db["benikui"]["datatexts"]["chat"]["enable"] = false
E.db["benikui"]["general"]["benikuiStyle"] = false
E.db["benikui"]["general"]["splashScreen"] = false
E.db["benikui"]["colors"]["colorTheme"] = "Hearthstone"
E.db["benikui"]["unitframes"]["player"]["detachPortrait"] = true
E.db["benikui"]["unitframes"]["player"]["portraitWidth"] = 105
E.db["benikui"]["unitframes"]["player"]["portraitHeight"] = 44
E.db["benikui"]["unitframes"]["target"]["detachPortrait"] = true
E.db["benikui"]["unitframes"]["target"]["portraitHeight"] = 44
E.db["benikui"]["unitframes"]["target"]["portraitWidth"] = 105

	--LAYOUT GOES HERE

	--[[
		--If you want to modify the base layout according to
		-- certain conditions then this is how you could do it --]]
		if layout == "universal" then
			--Make some changes to the layout posted above
			--Make some different changes
		end
	--]]


	--[[
	--	This section at the bottom is just to update ElvUI and display a message
	--]]
	--Update ElvUI
	E:UpdateAll(true)
	--Show message about layout being set
	PluginInstallStepComplete.message = "Installation Step Complete"
	PluginInstallStepComplete:Show()
end

--This function is executed when you press "Skip Process" or "Finished" in the installer.
local function InstallComplete()
	if GetCVarBool("Sound_EnableMusic") then
		StopMusic()
	end

	--Set a variable tracking the version of the addon when layout was installed
	E.db[MyPluginName].install_version = Version

	ReloadUI()
end

--This is the data we pass on to the ElvUI Plugin Installer.
--The Plugin Installer is reponsible for displaying the install guide for this layout.
local InstallerData = {
	Title = format("|cff4beb2c%s %s|r", MyPluginName, "Installation"),
	Name = MyPluginName,
	tutorialImage = "Interface\\AddOns\\pawsui\\Media\\logo.tga", --If you have a logo you want to use, otherwise it uses the one from ElvUI
	Pages = {
		[1] = function()
			PluginInstallFrame.SubTitle:SetFormattedText("Welcome to the installation for %s.", MyPluginName)
			PluginInstallFrame.Desc1:SetText("This installation process will guide you through a few steps and apply settings to the current ElvUI profile. What this means to you, is, when you pick through these options the UI will set itself up.")
			PluginInstallFrame.Desc2:SetText("Please press the continue button if you wish to go through the installation process, otherwise click the 'Skip Process' button.")
			PluginInstallFrame.Option1:Show()
			PluginInstallFrame.Option1:SetScript("OnClick", InstallComplete)
			PluginInstallFrame.Option1:SetText("Skip Process")
		end,
		[2] = function()
			PluginInstallFrame.SubTitle:SetText("Layouts")
			PluginInstallFrame.Desc1:SetText("These are the layouts that are available. Please click a button below to apply the layout of your choosing.")
			PluginInstallFrame.Desc2:SetText("Importance: |cff07D400High|r")
			PluginInstallFrame.Option1:Show()
			PluginInstallFrame.Option1:SetScript("OnClick", function() SetupLayout("universal") end)
			PluginInstallFrame.Option1:SetText("Universial")
		end,
		[3] = function()
			PluginInstallFrame.SubTitle:SetText("Installation Complete")
			PluginInstallFrame.Desc1:SetText("You have completed the installation process.")
			PluginInstallFrame.Desc2:SetText("Please click the button below in order to finalize the process and automatically reload your UI. You can find the latest update on the Paws United Discord.")
			PluginInstallFrame.Option1:Show()
			PluginInstallFrame.Option1:SetScript("OnClick", InstallComplete)
			PluginInstallFrame.Option1:SetText("Finished")
		end,
	},
	StepTitles = {
		[1] = "Welcome",
		[2] = "Layouts",
		[3] = "Installation Complete",
	},
	StepTitlesColor = {1, 1, 1},
	StepTitlesColorSelected = {0, 179/255, 1},
	StepTitleWidth = 200,
	StepTitleButtonWidth = 180,
	StepTitleTextJustification = "RIGHT",
}

--This function holds the options table which will be inserted into the ElvUI config
local function InsertOptions()
	E.Options.args.MyPluginName = {
		order = 100,
		type = "group",
		name = format("|cff4beb2c%s|r", MyPluginName),
		args = {
			header1 = {
				order = 1,
				type = "header",
				name = MyPluginName,
			},
			description1 = {
				order = 2,
				type = "description",
				name = format("%s is a layout for ElvUI.", MyPluginName),
			},
			spacer1 = {
				order = 3,
				type = "description",
				name = "\n\n\n",
			},
			header2 = {
				order = 4,
				type = "header",
				name = "Installation",
			},
			description2 = {
				order = 5,
				type = "description",
				name = "The installation guide should pop up automatically after you have completed the ElvUI installation. If you wish to re-run the installation process for this layout then please click the button below.",
			},
			spacer2 = {
				order = 6,
				type = "description",
				name = "",
			},
			install = {
				order = 7,
				type = "execute",
				name = "Install",
				desc = "Run the installation process.",
				func = function() E:GetModule("PluginInstaller"):Queue(InstallerData); E:ToggleConfig(); end,
			},
		},
	}
end

--Create a unique table for our plugin
P[MyPluginName] = {}

--This function will handle initialization of the addon
function mod:Initialize()
	--Initiate installation process if ElvUI install is complete and our plugin install has not yet been run
	if E.private.install_complete and E.db[MyPluginName].install_version == nil then
		E:GetModule("PluginInstaller"):Queue(InstallerData)
	end
	
	--Insert our options table when ElvUI config is loaded
	EP:RegisterPlugin(addon, InsertOptions)
end

--This function will get called by ElvUI automatically when it is ready to initialize modules
local function CallbackInitialize()
	mod:Initialize()
end

--Register module with callback so it gets initialized when ready
E:RegisterModule(MyPluginName, CallbackInitialize)